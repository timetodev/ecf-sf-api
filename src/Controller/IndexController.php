<?php

namespace App\Controller;

use App\Entity\Student;
use App\Entity\Promotion;
use App\Repository\StudentRepository;
use App\Repository\PromotionRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        // renvoi vers l'index de la page pour y afficher index.html.twig avec les deux boutton inscription et connexion 
       
        return $this->render('index/index.html.twig', [
            'controller_name' => 'IndexController',
        ]);
    }
}
