--------------------FRANCAIS----------------------

Pour installer cette outils , il vous faut 

    PHP 7.3 
    COMPOSER
    MariaDB 10.3.9
    Un Serveur Apache
    
Pour mettre en place Symfony , cloner ce répository sur votre serveur , 

Faite un composer install a la racine du projet 
Une foix fait il vous suffit de

    Modifier le .env pour les accées base de donnée 
    Faire un php bin/console doctrine:migrations:migrate pour y installer la base de donnée 

Voila le projet est installer , bonne utilisation.

------------------English-------------------------

To install this tool you need

PHP 7.3
COMPOSE
MariaDB 10.3.9
An Apache Server

To set up Symfony, clone this repository on your server,

Makes a composer install at the root of the project
A Foix does you just have to

Edit the .env for the database access
Make a php bin/console doctrine:migrations:migrate to install  the database

This is the project is installing , good use.